const Product = require('../models/Product');

module.exports.addProduct = async (data) => {
	let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});
	let checkProduct = await Product.findOne({ name: data.product.name })
	if (checkProduct !== null) {
		return false
	}
	return newProduct.save().then((product, error) => {
		if (error) {
			return false
		} else {
			return true
		};
	});
}
module.exports.getAllActive = async () => {
	try {
		let products = await Product.find({ isActive: true });
		return products;
	} catch (err) {
		console.log(err);
		return `There's a problem in your request.`
	}
}

module.exports.getAllProducts = async () => {
	try {
		let products = await Product.find({});
		return products;
	} catch (err) {
		console.log(err);
		return `There's a problem in your request.`
	}
}

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result
	})
}

module.exports.updateProduct = (data) => {
	return Product.findById(data.productId).then((result, err) => {

		result.name = data.updatedProductInfo.name,
			result.description = data.updatedProductInfo.description,
			result.price = data.updatedProductInfo.price

		return result.save().then((updatedProductInfo, err) => {
			if (err) {
				return `Updating not successful. Please try again.`
			} else {
				return updatedProductInfo
			}
		})
	})
}

module.exports.archiveProduct = (data) => {
	return Product.findById(data.productId).then((result, err) => {
		result.isActive = !data.archivedProduct.isActive;

		return result.save().then((archiveProduct, err) => {
			if (err) {
				return false
			} else {
				return true
			}
		})
	})
}