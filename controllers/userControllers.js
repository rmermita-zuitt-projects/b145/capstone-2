const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	return User.find({ email: reqBody.email }).then(result => {
		if (result.length > 0) {
			return {
				title: "Email already exists",
				text: "Please provide another email",
				success: false
			}
		}
		let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password, 10)
		})
		return newUser.save().then((user, err) => {
			if (err) {
				console.log(err)
				return {
					title: 'Something went wrong',
					text: 'Please try again.',
					success: false
				}
			} else {
				return {
					title: 'Registration successful',
					text: 'Welcome to BTS Shop!',
					success: true
				}
			}
		})
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {
		if (result == null) {
			return `Can't find user!`
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return `Incorrect password!`
			}
		}
	})
}

module.exports.updateAdminRole = (data) => {
	return User.findById(data.userId).then((result, err) => {
		result.isAdmin = data.isAdmin;

		return result.save().then((user, err) => {
			if (err) {
				return `Something went wrong!`
			} else {
				return user;
			}
		})
	})
}

module.exports.allUsers = async () => {
	try {
		let users = await User.find({});
		return users;
	} catch (err) {
		console.log(err);
		return `There's a problem in your request.`
	}
}

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		// Returns the user information with the password as an empty string
		return result;

	});

};