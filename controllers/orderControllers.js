const Order = require('../models/Order');
const Product = require('../models/Product');

module.exports.addToCart = async (data) => {
	try {
		let cart = await Order.findOne({ userId: data.userId, purchasedOn: null });
		if (cart === null) {
			let newCart = new Order({
				userId: data.userId,
				products: [data.productDetails]
			})
			await newCart.save();
			return newCart;
		} else {
			let isProductExists = false
			let removeIndex = null
			for (let i = 0; i < cart.products.length; i++) {
				if (cart.products[i].productId.toString() === data.productDetails.productId) {
					if (data.productDetails.quantity === 0) {
						removeIndex = i;
					} else {
						cart.products[i].quantity = data.productDetails.quantity
					}
					isProductExists = true
					break;
				}
			}
			if (!isProductExists) {
				cart.products.push(data.productDetails)
			} else if (data.productDetails.quantity === 0) {
				cart.products.splice(removeIndex, 1);
			}
			await cart.save();
			return cart;
		}
	} catch (err) {
		console.log(err);
		return `Failed adding or updating item to cart`
	}
}

module.exports.checkout = async (data) => {
	try {
		let cart = await Order.findOne({ userId: data.userId, purchasedOn: null });
		if (cart === null) {
			return `Your cart is empty.`
		}
		cart.purchasedOn = new Date();
		let totalAmt = 0
		for (let i = 0; i < cart.products.length; i++) {
			let product = await Product.findById(cart.products[i].productId);
			totalAmt += (product.price * cart.products[i].quantity)
		}
		cart.totalAmount = totalAmt
		await cart.save();
		return true;
	} catch (err) {
		console.log(err);
		return `Failed checking out your cart`
	}
}

module.exports.userOrders = async (data) => {
	try {
		let orders = await Order.find({ userId: data.userId, purchasedOn: { $ne: null } });
		return orders;
	} catch (err) {
		console.log(err);
		return `There's a problem in your request.`
	}
}

module.exports.allOrders = async () => {
	try {
		let orders = await Order.find({ purchasedOn: { $ne: null } });
		return orders;
	} catch (err) {
		console.log(err);
		return `There's a problem in your request.`
	}
}

module.exports.getCart = async (data) => {
	try {
		const cart = await Order.findOne({
			userId: data.userId,
			purchasedOn: null
		});
		if (cart === null) {
			return {
				cartProducts: []
			}
		}
		let cartProducts = []
		for (let i = 0; i < cart.products.length; i++) {
			let product = await Product.findById(cart.products[i].productId);
			cartProducts.push({
				name: product.name,
				productId: product._id,
				quantity: cart.products[i].quantity,
				price: product.price,
				subtotal: product.price * cart.products[i].quantity
			})
		}
		return {
			cartProducts
		};
	} catch (err) {
		console.log(err);
		return {
			cartProducts: []
		}
	}
}
