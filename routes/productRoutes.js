const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth')

router.post("/add-product", auth.verify, (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`User not authorized.`)
		return;
	}
	const data = {
		product: req.body,
		payload: auth.decode(req.headers.authorization)
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
});

router.get('/', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

router.get('/all', auth.verify, (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`User not authorized.`)
		return;
	}
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});


router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

router.put('/:productId', auth.verify, (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`User not authorized.`)
		return;
	}
	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization),
		updatedProductInfo: req.body
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))

});

router.put('/:productId/archive', auth.verify, (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`User not authorized.`)
		return;
	}
	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization),
		archivedProduct: req.body
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;