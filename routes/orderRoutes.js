const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth = require('../auth')

router.post('/add-to-cart', auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
		res.send(`This is for non-admin account only.`)
		return;
	}
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productDetails: req.body
	}
	orderController.addToCart(data).then(resultFromController => res.send(resultFromController))
});

router.put('/checkout', auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
		res.send(`This is for non-admin users only.`)
		return;
	}
	const data = {
		userId: auth.decode(req.headers.authorization).id
	}
	orderController.checkout(data).then(resultFromController => res.send(resultFromController))
});

router.get('/', auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are not authorized to view this user's order(s)`)
		return;
	}
	const data = {
		userId: auth.decode(req.headers.authorization).id
	}
	orderController.userOrders(data).then(resultFromController => res.send(resultFromController))
});

router.get('/all', auth.verify, (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are not allowed to view this section.`)
		return;
	}
	orderController.allOrders().then(resultFromController => res.send(resultFromController))
})

router.get('/cart', auth.verify, (req, res) => {
	if (auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are not allowed to view this section.`)
		return;
	}
	const data = {
		userId: auth.decode(req.headers.authorization).id
	}
	orderController.getCart(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;