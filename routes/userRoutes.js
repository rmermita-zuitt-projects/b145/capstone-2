const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post('/register', (req, res) => {
	if (req.body.password !== req.body.verifyPassword) {
		res.send({
			title: `Password did not match`,
			text: `Please try again`,
			success: false
		})
		return
	}
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.put('/admin', (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are unauthorized to edit this.`)
		return;
	}
	const data = {
		userId: req.body.userId,
		isAdmin: req.body.isAdmin
	}
	userController.updateAdminRole(data).then(resultFromController => res.send(resultFromController))
});

router.get('/all', auth.verify, (req, res) => {
	if (!auth.decode(req.headers.authorization).isAdmin) {
		res.send(`You are not an admin.`)
		return;
	}
	userController.allUsers().then(resultFromController => res.send(resultFromController))
});

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));

});

module.exports = router;