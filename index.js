//[SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

//[SECTION] Environment Variables Setup
dotenv.config();
const secret = process.env.CONNECTION_STRING;

//[SECTION] Server Setup
const app = express();
const port = process.env.PORT || 5000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());



//[SECTION] Database Connect
mongoose.connect(secret,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

let db = mongoose.connection

db.on('error', () => console.error.bind(console, 'error'))
db.once('open', () => console.log('Successfully connected to MongoDB'))


//[SECTION] Server Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

//[SECTION] Server Response
app.get('/', (req, res) => {
	res.send('Hosted in Heroku')
})
app.listen(port, () => console.log(`Server is running at port ${port}`))
